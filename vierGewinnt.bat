@echo off
:title
echo   --------------
echo  / Vier Gewinnt \
echo  ----------------
echo.

:names
echo Enter name of player 1
set /p p1=
echo Enter name of player 2
set /p p2=

rem todo: computer gegner
:variables
set f11= 
set f12= 
set f13= 
set f14= 
set f15= 
set f16= 
set f17= 
set f18= 
set f21= 
set f22= 
set f23= 
set f24= 
set f25= 
set f26= 
set f27= 
set f28= 
set f31= 
set f32= 
set f33= 
set f34= 
set f35= 
set f36= 
set f37= 
set f38= 
set f41= 
set f42= 
set f43= 
set f44= 
set f45= 
set f46= 
set f47= 
set f48= 
set f51= 
set f52= 
set f53= 
set f54= 
set f55= 
set f56= 
set f57= 
set f58= 
set f61= 
set f62= 
set f63= 
set f64= 
set f65= 
set f66= 
set f67= 
set f68= 
set f71= 
set f72= 
set f73= 
set f74= 
set f75= 
set f76= 
set f77= 
set f78= 
set f81= 
set f82= 
set f83= 
set f84= 
set f85= 
set f86= 
set f87= 
set f88= 
set turn=1

:game
cls
color 07
echo %p1% X vs O %p2%
echo.
echo    1   2   3   4   5   6   7   8
echo  ^| %f18% ^| %f28% ^| %f38% ^| %f48% ^| %f58% ^| %f68% ^| %f78% ^| %f88% ^|
echo  ^|   ^|   ^|   ^|   ^|   ^|   ^|   ^|   ^|
echo  ^| %f17% ^| %f27% ^| %f37% ^| %f47% ^| %f57% ^| %f67% ^| %f77% ^| %f87% ^|
echo  ^|   ^|   ^|   ^|   ^|   ^|   ^|   ^|   ^|
echo  ^| %f16% ^| %f26% ^| %f36% ^| %f46% ^| %f56% ^| %f66% ^| %f76% ^| %f86% ^|
echo  ^|   ^|   ^|   ^|   ^|   ^|   ^|   ^|   ^|
echo  ^| %f15% ^| %f25% ^| %f35% ^| %f45% ^| %f55% ^| %f65% ^| %f75% ^| %f85% ^|
echo  ^|   ^|   ^|   ^|   ^|   ^|   ^|   ^|   ^|
echo  ^| %f14% ^| %f24% ^| %f34% ^| %f44% ^| %f54% ^| %f64% ^| %f74% ^| %f84% ^|
echo  ^|   ^|   ^|   ^|   ^|   ^|   ^|   ^|   ^|
echo  ^| %f13% ^| %f23% ^| %f33% ^| %f43% ^| %f53% ^| %f63% ^| %f73% ^| %f83% ^|
echo  ^|   ^|   ^|   ^|   ^|   ^|   ^|   ^|   ^|
echo  ^| %f12% ^| %f22% ^| %f32% ^| %f42% ^| %f52% ^| %f62% ^| %f72% ^| %f82% ^|
echo  ^|   ^|   ^|   ^|   ^|   ^|   ^|   ^|   ^|
echo  ^| %f11% ^| %f21% ^| %f31% ^| %f41% ^| %f51% ^| %f61% ^| %f71% ^| %f81% ^|
echo   -------------------------------
echo.
goto checkVictory

:turnPlayer1
color 17
echo %p1% X places
echo Select column (1-8)
set /p col=
if "%col%"=="1" ( if "%f18%"==" " ( set f18=X 
if "%f17%"==" " (
set f18= 
set f17=X
if "%f16%"==" " (
set f17= 
set f16=X
if "%f15%"==" " (
set f16= 
set f15=X
if "%f14%"==" " (
set f15= 
set f14=X
if "%f13%"==" " (
set f14= 
set f13=X
if "%f12%"==" " (
set f13= 
set f12=X
if "%f11%"==" " (
set f12= 
set f11=X
)
)
)
)
)
)
)
)
)
if "%col%"=="2" ( if "%f28%"==" " ( set f28=X
if "%f27%"==" " (
set f28= 
set f27=X
if "%f26%"==" " (
set f27= 
set f26=X
if "%f25%"==" " (
set f26= 
set f25=X
if "%f24%"==" " (
set f25= 
set f24=X
if "%f23%"==" " (
set f24= 
set f23=X
if "%f22%"==" " (
set f23= 
set f22=X
if "%f21%"==" " (
set f22= 
set f21=X
)
)
)
)
)
)
)
)
)
if "%col%"=="3" ( if "%f38%"==" " ( set f38=X
if "%f37%"==" " (
set f38= 
set f37=X
if "%f36%"==" " (
set f37= 
set f36=X
if "%f35%"==" " (
set f36= 
set f35=X
if "%f34%"==" " (
set f35= 
set f34=X
if "%f33%"==" " (
set f34= 
set f33=X
if "%f32%"==" " (
set f33= 
set f32=X
if "%f31%"==" " (
set f32= 
set f31=X
)
)
)
)
)
)
)
)
)
if "%col%"=="4" ( if "%f48%"==" " ( set f48=X
if "%f47%"==" " (
set f48= 
set f47=X
if "%f46%"==" " (
set f47= 
set f46=X
if "%f45%"==" " (
set f46= 
set f45=X
if "%f44%"==" " (
set f45= 
set f44=X
if "%f43%"==" " (
set f44= 
set f43=X
if "%f42%"==" " (
set f43= 
set f42=X
if "%f41%"==" " (
set f42= 
set f41=X
)
)
)
)
)
)
)
)
)
if "%col%"=="5" ( if "%f58%"==" " ( set f58=X
if "%f57%"==" " (
set f58= 
set f57=X
if "%f56%"==" " (
set f57= 
set f56=X
if "%f55%"==" " (
set f56= 
set f55=X
if "%f54%"==" " (
set f55= 
set f54=X
if "%f53%"==" " (
set f54= 
set f53=X
if "%f52%"==" " (
set f53= 
set f52=X
if "%f51%"==" " (
set f52= 
set f51=X
)
)
)
)
)
)
)
)
)
if "%col%"=="6" ( if "%f68%"==" " ( set f68=X
if "%f67%"==" " (
set f68= 
set f67=X
if "%f66%"==" " (
set f67= 
set f66=X
if "%f65%"==" " (
set f66= 
set f65=X
if "%f64%"==" " (
set f65= 
set f64=X
if "%f63%"==" " (
set f64= 
set f63=X
if "%f62%"==" " (
set f63= 
set f62=X
if "%f61%"==" " (
set f62= 
set f61=X
)
)
)
)
)
)
)
)
)
if "%col%"=="7" ( if "%f78%"==" " ( set f78=X
if "%f77%"==" " (
set f78= 
set f77=X
if "%f76%"==" " (
set f77= 
set f76=X
if "%f75%"==" " (
set f76= 
set f75=X
if "%f74%"==" " (
set f75= 
set f74=X
if "%f73%"==" " (
set f74= 
set f73=X
if "%f72%"==" " (
set f73= 
set f72=X
if "%f71%"==" " (
set f72= 
set f71=X
)
)
)
)
)
)
)
)
)
if "%col%"=="8" ( if "%f88%"==" " ( set f88=X
if "%f87%"==" " (
set f88= 
set f87=X
if "%f86%"==" " (
set f87= 
set f86=X
if "%f85%"==" " (
set f86= 
set f85=X
if "%f84%"==" " (
set f85= 
set f84=X
if "%f83%"==" " (
set f84= 
set f83=X
if "%f82%"==" " (
set f83= 
set f82=X
if "%f81%"==" " (
set f82= 
set f81=X
)
)
)
)
)
)
)
)
)
set turn=2
goto game


:turnPlayer2
color 47
echo %p2% O places
echo Select column
set /p col=
if "%col%"=="1" ( if "%f18%"==" " ( set f18=O
if "%f17%"==" " (
set f18= 
set f17=O
if "%f16%"==" " (
set f17= 
set f16=O
if "%f15%"==" " (
set f16= 
set f15=O
if "%f14%"==" " (
set f15= 
set f14=O
if "%f13%"==" " (
set f14= 
set f13=O
if "%f12%"==" " (
set f13= 
set f12=O
if "%f11%"==" " (
set f12= 
set f11=O
)
)
)
)
)
)
)
)
)
if "%col%"=="2" ( if "%f28%"==" " ( set f28=O
if "%f27%"==" " (
set f28= 
set f27=O
if "%f26%"==" " (
set f27= 
set f26=O
if "%f25%"==" " (
set f26= 
set f25=O
if "%f24%"==" " (
set f25= 
set f24=O
if "%f23%"==" " (
set f24= 
set f23=O
if "%f22%"==" " (
set f23= 
set f22=O
if "%f21%"==" " (
set f22= 
set f21=O
)
)
)
)
)
)
)
)
)
if "%col%"=="3" ( if "%f38%"==" " ( set f38=O
if "%f37%"==" " (
set f38= 
set f37=O
if "%f36%"==" " (
set f37= 
set f36=O
if "%f35%"==" " (
set f36= 
set f35=O
if "%f34%"==" " (
set f35= 
set f34=O
if "%f33%"==" " (
set f34= 
set f33=O
if "%f32%"==" " (
set f33= 
set f32=O
if "%f31%"==" " (
set f32= 
set f31=O
)
)
)
)
)
)
)
)
)
if "%col%"=="4" ( if "%f48%"==" " ( set f48=O
if "%f47%"==" " (
set f48= 
set f47=O
if "%f46%"==" " (
set f47= 
set f46=O
if "%f45%"==" " (
set f46= 
set f45=O
if "%f44%"==" " (
set f45= 
set f44=O
if "%f43%"==" " (
set f44= 
set f43=O
if "%f42%"==" " (
set f43= 
set f42=O
if "%f41%"==" " (
set f42= 
set f41=O
)
)
)
)
)
)
)
)
)
if "%col%"=="5" ( if "%f58%"==" " ( set f58=O
if "%f57%"==" " (
set f58= 
set f57=O
if "%f56%"==" " (
set f57= 
set f56=O
if "%f55%"==" " (
set f56= 
set f55=O
if "%f54%"==" " (
set f55= 
set f54=O
if "%f53%"==" " (
set f54= 
set f53=O
if "%f52%"==" " (
set f53= 
set f52=O
if "%f51%"==" " (
set f52= 
set f51=O
)
)
)
)
)
)
)
)
)
if "%col%"=="6" ( if "%f68%"==" " ( set f68=O
if "%f67%"==" " (
set f68= 
set f67=O
if "%f66%"==" " (
set f67= 
set f66=O
if "%f65%"==" " (
set f66= 
set f65=O
if "%f64%"==" " (
set f65= 
set f64=O
if "%f63%"==" " (
set f64= 
set f63=O
if "%f62%"==" " (
set f63= 
set f62=O
if "%f61%"==" " (
set f62= 
set f61=O
)
)
)
)
)
)
)
)
)
if "%col%"=="7" ( if "%f78%"==" " ( set f78=O
if "%f77%"==" " (
set f78= 
set f77=O
if "%f76%"==" " (
set f77= 
set f76=O
if "%f75%"==" " (
set f76= 
set f75=O
if "%f74%"==" " (
set f75= 
set f74=O
if "%f73%"==" " (
set f74= 
set f73=O
if "%f72%"==" " (
set f73= 
set f72=O
if "%f71%"==" " (
set f72= 
set f71=O
)
)
)
)
)
)
)
)
)
if "%col%"=="8" ( if "%f88%"==" " ( set f88=O
if "%f87%"==" " (
set f88= 
set f87=O
if "%f86%"==" " (
set f87= 
set f86=O
if "%f85%"==" " (
set f86= 
set f85=O
if "%f84%"==" " (
set f85= 
set f84=O
if "%f83%"==" " (
set f84= 
set f83=O
if "%f82%"==" " (
set f83= 
set f82=O
if "%f81%"==" " (
set f82= 
set f81=O
)
)
)
)
)
)
)
)
)
set turn=1
goto game

:victory1
echo.
echo.
echo Congratulations %p1%
echo You defeated your opponent
pause > nul
goto exit

:victory2
echo.
echo.
echo Congratulations %p2%
echo You defeated your opponent
pause > nul
goto exit

:draw
echo.
echo.
echo A tie!
pause > nul
goto exit

:checkVictory
rem Player 1
if "%f11%"=="X" ( if "%f12%"=="X" ( if "%f13%"=="X" ( if "%f14%"=="X" goto victory1 
)
)
)
if "%f12%"=="X" ( if "%f13%"=="X" ( if "%f14%"=="X" ( if "%f15%"=="X" goto victory1 
)
)
)
if "%f13%"=="X" ( if "%f14%"=="X" ( if "%f15%"=="X" ( if "%f16%"=="X" goto victory1 
)
)
)
if "%f14%"=="X" ( if "%f15%"=="X" ( if "%f16%"=="X" ( if "%f17%"=="X" goto victory1 
)
)
)
if "%f15%"=="X" ( if "%f16%"=="X" ( if "%f17%"=="X" ( if "%f18%"=="X" goto victory1 
)
)
)
if "%f21%"=="X" ( if "%f22%"=="X" ( if "%f23%"=="X" ( if "%f24%"=="X" goto victory1 
)
)
)
if "%f22%"=="X" ( if "%f23%"=="X" ( if "%f24%"=="X" ( if "%f25%"=="X" goto victory1 
)
)
)
if "%f23%"=="X" ( if "%f24%"=="X" ( if "%f25%"=="X" ( if "%f26%"=="X" goto victory1 
)
)
)
if "%f24%"=="X" ( if "%f25%"=="X" ( if "%f26%"=="X" ( if "%f27%"=="X" goto victory1 
)
)
)
if "%f25%"=="X" ( if "%f26%"=="X" ( if "%f27%"=="X" ( if "%f28%"=="X" goto victory1 
)
)
)
if "%f31%"=="X" ( if "%f32%"=="X" ( if "%f33%"=="X" ( if "%f34%"=="X" goto victory1 
)
)
)
if "%f32%"=="X" ( if "%f33%"=="X" ( if "%f34%"=="X" ( if "%f35%"=="X" goto victory1 
)
)
)
if "%f33%"=="X" ( if "%f34%"=="X" ( if "%f35%"=="X" ( if "%f36%"=="X" goto victory1 
)
)
)
if "%f34%"=="X" ( if "%f35%"=="X" ( if "%f36%"=="X" ( if "%f37%"=="X" goto victory1 
)
)
)
if "%f35%"=="X" ( if "%f36%"=="X" ( if "%f37%"=="X" ( if "%f38%"=="X" goto victory1 
)
)
)
if "%f41%"=="X" ( if "%f42%"=="X" ( if "%f43%"=="X" ( if "%f44%"=="X" goto victory1 
)
)
)
if "%f42%"=="X" ( if "%f43%"=="X" ( if "%f44%"=="X" ( if "%f45%"=="X" goto victory1 
)
)
)
if "%f43%"=="X" ( if "%f44%"=="X" ( if "%f45%"=="X" ( if "%f46%"=="X" goto victory1 
)
)
)
if "%f44%"=="X" ( if "%f45%"=="X" ( if "%f46%"=="X" ( if "%f47%"=="X" goto victory1 
)
)
)
if "%f45%"=="X" ( if "%f46%"=="X" ( if "%f47%"=="X" ( if "%f48%"=="X" goto victory1 
)
)
)
if "%f51%"=="X" ( if "%f52%"=="X" ( if "%f53%"=="X" ( if "%f54%"=="X" goto victory1 
)
)
)
if "%f52%"=="X" ( if "%f53%"=="X" ( if "%f54%"=="X" ( if "%f55%"=="X" goto victory1 
)
)
)
if "%f53%"=="X" ( if "%f54%"=="X" ( if "%f55%"=="X" ( if "%f56%"=="X" goto victory1 
)
)
)
if "%f54%"=="X" ( if "%f55%"=="X" ( if "%f56%"=="X" ( if "%f57%"=="X" goto victory1 
)
)
)
if "%f55%"=="X" ( if "%f56%"=="X" ( if "%f57%"=="X" ( if "%f58%"=="X" goto victory1 
)
)
)
if "%f61%"=="X" ( if "%f62%"=="X" ( if "%f63%"=="X" ( if "%f64%"=="X" goto victory1 
)
)
)
if "%f62%"=="X" ( if "%f63%"=="X" ( if "%f64%"=="X" ( if "%f65%"=="X" goto victory1 
)
)
)
if "%f63%"=="X" ( if "%f64%"=="X" ( if "%f65%"=="X" ( if "%f66%"=="X" goto victory1 
)
)
)
if "%f64%"=="X" ( if "%f65%"=="X" ( if "%f66%"=="X" ( if "%f67%"=="X" goto victory1 
)
)
)
if "%f65%"=="X" ( if "%f66%"=="X" ( if "%f67%"=="X" ( if "%f68%"=="X" goto victory1 
)
)
)
if "%f71%"=="X" ( if "%f72%"=="X" ( if "%f73%"=="X" ( if "%f74%"=="X" goto victory1 
)
)
)
if "%f72%"=="X" ( if "%f73%"=="X" ( if "%f74%"=="X" ( if "%f75%"=="X" goto victory1 
)
)
)
if "%f73%"=="X" ( if "%f74%"=="X" ( if "%f75%"=="X" ( if "%f76%"=="X" goto victory1 
)
)
)
if "%f74%"=="X" ( if "%f75%"=="X" ( if "%f76%"=="X" ( if "%f77%"=="X" goto victory1 
)
)
)
if "%f75%"=="X" ( if "%f76%"=="X" ( if "%f77%"=="X" ( if "%f78%"=="X" goto victory1 
)
)
)
if "%f81%"=="X" ( if "%f82%"=="X" ( if "%f83%"=="X" ( if "%f84%"=="X" goto victory1 
)
)
)
if "%f82%"=="X" ( if "%f83%"=="X" ( if "%f84%"=="X" ( if "%f85%"=="X" goto victory1 
)
)
)
if "%f83%"=="X" ( if "%f84%"=="X" ( if "%f85%"=="X" ( if "%f86%"=="X" goto victory1 
)
)
)
if "%f84%"=="X" ( if "%f85%"=="X" ( if "%f86%"=="X" ( if "%f87%"=="X" goto victory1 
)
)
)
if "%f85%"=="X" ( if "%f86%"=="X" ( if "%f87%"=="X" ( if "%f88%"=="X" goto victory1 
)
)
)
if "%f11%"=="X" ( if "%f21%"=="X" ( if "%f31%"=="X" ( if "%f41%"=="X" goto victory1 
)
)
)
if "%f12%"=="X" ( if "%f22%"=="X" ( if "%f32%"=="X" ( if "%f42%"=="X" goto victory1 
)
)
)
if "%f13%"=="X" ( if "%f23%"=="X" ( if "%f33%"=="X" ( if "%f43%"=="X" goto victory1 
)
)
)
if "%f14%"=="X" ( if "%f24%"=="X" ( if "%f34%"=="X" ( if "%f44%"=="X" goto victory1 
)
)
)
if "%f15%"=="X" ( if "%f25%"=="X" ( if "%f35%"=="X" ( if "%f45%"=="X" goto victory1 
)
)
)
if "%f16%"=="X" ( if "%f26%"=="X" ( if "%f36%"=="X" ( if "%f46%"=="X" goto victory1 
)
)
)
if "%f17%"=="X" ( if "%f27%"=="X" ( if "%f37%"=="X" ( if "%f47%"=="X" goto victory1 
)
)
)
if "%f18%"=="X" ( if "%f28%"=="X" ( if "%f38%"=="X" ( if "%f48%"=="X" goto victory1 
)
)
)
if "%f51%"=="X" ( if "%f61%"=="X" ( if "%f71%"=="X" ( if "%f81%"=="X" goto victory1 
)
)
)
if "%f52%"=="X" ( if "%f62%"=="X" ( if "%f72%"=="X" ( if "%f82%"=="X" goto victory1 
)
)
)
if "%f53%"=="X" ( if "%f63%"=="X" ( if "%f73%"=="X" ( if "%f83%"=="X" goto victory1 
)
)
)
if "%f54%"=="X" ( if "%f64%"=="X" ( if "%f74%"=="X" ( if "%f84%"=="X" goto victory1 
)
)
)
if "%f55%"=="X" ( if "%f65%"=="X" ( if "%f75%"=="X" ( if "%f85%"=="X" goto victory1 
)
)
)
if "%f56%"=="X" ( if "%f66%"=="X" ( if "%f76%"=="X" ( if "%f86%"=="X" goto victory1 
)
)
)
if "%f57%"=="X" ( if "%f67%"=="X" ( if "%f77%"=="X" ( if "%f87%"=="X" goto victory1 
)
)
)
if "%f58%"=="X" ( if "%f68%"=="X" ( if "%f78%"=="X" ( if "%f88%"=="X" goto victory1 
)
)
)
if "%f11%"=="X" ( if "%f22%"=="X" ( if "%f33%"=="X" ( if "%f44%"=="X" goto victory1 
)
)
)
if "%f22%"=="X" ( if "%f33%"=="X" ( if "%f44%"=="X" ( if "%f55%"=="X" goto victory1 
)
)
)
if "%f33%"=="X" ( if "%f44%"=="X" ( if "%f55%"=="X" ( if "%f66%"=="X" goto victory1 
)
)
)
if "%f44%"=="X" ( if "%f55%"=="X" ( if "%f66%"=="X" ( if "%f77%"=="X" goto victory1 
)
)
)
if "%f55%"=="X" ( if "%f66%"=="X" ( if "%f77%"=="X" ( if "%f88%"=="X" goto victory1 
)
)
)
if "%f12%"=="X" ( if "%f23%"=="X" ( if "%f34%"=="X" ( if "%f45%"=="X" goto victory1 
)
)
)
if "%f23%"=="X" ( if "%f34%"=="X" ( if "%f45%"=="X" ( if "%f56%"=="X" goto victory1 
)
)
)
if "%f34%"=="X" ( if "%f45%"=="X" ( if "%f56%"=="X" ( if "%f67%"=="X" goto victory1 
)
)
)
if "%f45%"=="X" ( if "%f56%"=="X" ( if "%f67%"=="X" ( if "%f78%"=="X" goto victory1 
)
)
)
if "%f13%"=="X" ( if "%f24%"=="X" ( if "%f35%"=="X" ( if "%f46%"=="X" goto victory1 
)
)
)
if "%f24%"=="X" ( if "%f35%"=="X" ( if "%f46%"=="X" ( if "%f57%"=="X" goto victory1 
)
)
)
if "%f35%"=="X" ( if "%f46%"=="X" ( if "%f57%"=="X" ( if "%f68%"=="X" goto victory1 
)
)
)
if "%f14%"=="X" ( if "%f25%"=="X" ( if "%f36%"=="X" ( if "%f47%"=="X" goto victory1 
)
)
)
if "%f25%"=="X" ( if "%f36%"=="X" ( if "%f47%"=="X" ( if "%f58%"=="X" goto victory1 
)
)
)
if "%f15%"=="X" ( if "%f26%"=="X" ( if "%f37%"=="X" ( if "%f48%"=="X" goto victory1 
)
)
)
if "%f21%"=="X" ( if "%f32%"=="X" ( if "%f43%"=="X" ( if "%f54%"=="X" goto victory1 
)
)
)
if "%f32%"=="X" ( if "%f43%"=="X" ( if "%f54%"=="X" ( if "%f65%"=="X" goto victory1 
)
)
)
if "%f43%"=="X" ( if "%f54%"=="X" ( if "%f65%"=="X" ( if "%f76%"=="X" goto victory1 
)
)
)
if "%f54%"=="X" ( if "%f65%"=="X" ( if "%f76%"=="X" ( if "%f87%"=="X" goto victory1 
)
)
)
if "%f31%"=="X" ( if "%f42%"=="X" ( if "%f53%"=="X" ( if "%f64%"=="X" goto victory1 
)
)
)
if "%f42%"=="X" ( if "%f53%"=="X" ( if "%f64%"=="X" ( if "%f75%"=="X" goto victory1 
)
)
)
if "%f53%"=="X" ( if "%f64%"=="X" ( if "%f75%"=="X" ( if "%f86%"=="X" goto victory1 
)
)
)
if "%f32%"=="X" ( if "%f43%"=="X" ( if "%f54%"=="X" ( if "%f65%"=="X" goto victory1 
)
)
)
if "%f43%"=="X" ( if "%f54%"=="X" ( if "%f65%"=="X" ( if "%f76%"=="X" goto victory1 
)
)
)
if "%f54%"=="X" ( if "%f65%"=="X" ( if "%f76%"=="X" ( if "%f87%"=="X" goto victory1 
)
)
)
if "%f41%"=="X" ( if "%f52%"=="X" ( if "%f63%"=="X" ( if "%f74%"=="X" goto victory1 
)
)
)
if "%f52%"=="X" ( if "%f63%"=="X" ( if "%f74%"=="X" ( if "%f85%"=="X" goto victory1 
)
)
)
if "%f51%"=="X" ( if "%f62%"=="X" ( if "%f73%"=="X" ( if "%f84%"=="X" goto victory1 
)
)
)
if "%f81%"=="X" ( if "%f72%"=="X" ( if "%f63%"=="X" ( if "%f54%"=="X" goto victory1 
)
)
)
if "%f72%"=="X" ( if "%f63%"=="X" ( if "%f54%"=="X" ( if "%f45%"=="X" goto victory1 
)
)
)
if "%f63%"=="X" ( if "%f54%"=="X" ( if "%f45%"=="X" ( if "%f36%"=="X" goto victory1 
)
)
)
if "%f54%"=="X" ( if "%f45%"=="X" ( if "%f36%"=="X" ( if "%f27%"=="X" goto victory1 
)
)
)
if "%f45%"=="X" ( if "%f36%"=="X" ( if "%f27%"=="X" ( if "%f18%"=="X" goto victory1 
)
)
)
if "%f82%"=="X" ( if "%f73%"=="X" ( if "%f64%"=="X" ( if "%f55%"=="X" goto victory1 
)
)
)
if "%f73%"=="X" ( if "%f64%"=="X" ( if "%f55%"=="X" ( if "%f46%"=="X" goto victory1 
)
)
)
if "%f64%"=="X" ( if "%f55%"=="X" ( if "%f46%"=="X" ( if "%f37%"=="X" goto victory1 
)
)
)
if "%f55%"=="X" ( if "%f46%"=="X" ( if "%f37%"=="X" ( if "%f28%"=="X" goto victory1 
)
)
)
if "%f83%"=="X" ( if "%f74%"=="X" ( if "%f65%"=="X" ( if "%f56%"=="X" goto victory1 
)
)
)
if "%f74%"=="X" ( if "%f65%"=="X" ( if "%f56%"=="X" ( if "%f47%"=="X" goto victory1 
)
)
)
if "%f65%"=="X" ( if "%f56%"=="X" ( if "%f47%"=="X" ( if "%f38%"=="X" goto victory1 
)
)
)
if "%f84%"=="X" ( if "%f75%"=="X" ( if "%f66%"=="X" ( if "%f57%"=="X" goto victory1 
)
)
)
if "%f75%"=="X" ( if "%f66%"=="X" ( if "%f57%"=="X" ( if "%f48%"=="X" goto victory1 
)
)
)
if "%f85%"=="X" ( if "%f76%"=="X" ( if "%f67%"=="X" ( if "%f58%"=="X" goto victory1 
)
)
)
if "%f71%"=="X" ( if "%f62%"=="X" ( if "%f53%"=="X" ( if "%f44%"=="X" goto victory1 
)
)
)
if "%f62%"=="X" ( if "%f53%"=="X" ( if "%f44%"=="X" ( if "%f35%"=="X" goto victory1 
)
)
)
if "%f53%"=="X" ( if "%f44%"=="X" ( if "%f35%"=="X" ( if "%f26%"=="X" goto victory1 
)
)
)
if "%f44%"=="X" ( if "%f35%"=="X" ( if "%f26%"=="X" ( if "%f17%"=="X" goto victory1 
)
)
)
if "%f61%"=="X" ( if "%f52%"=="X" ( if "%f43%"=="X" ( if "%f34%"=="X" goto victory1 
)
)
)
if "%f52%"=="X" ( if "%f43%"=="X" ( if "%f35%"=="X" ( if "%f26%"=="X" goto victory1 
)
)
)
if "%f43%"=="X" ( if "%f34%"=="X" ( if "%f25%"=="X" ( if "%f16%"=="X" goto victory1 
)
)
)
if "%f51%"=="X" ( if "%f42%"=="X" ( if "%f33%"=="X" ( if "%f24%"=="X" goto victory1 
)
)
)
if "%f42%"=="X" ( if "%f33%"=="X" ( if "%f24%"=="X" ( if "%f15%"=="X" goto victory1 
)
)
)
if "%f41%"=="X" ( if "%f32%"=="X" ( if "%f23%"=="X" ( if "%f14%"=="X" goto victory1 
)
)
)
rem Player 2
if "%f11%"=="O" ( if "%f12%"=="O" ( if "%f13%"=="O" ( if "%f14%"=="O" goto victory2 
)
)
)
if "%f12%"=="O" ( if "%f13%"=="O" ( if "%f14%"=="O" ( if "%f15%"=="O" goto victory2 
)
)
)
if "%f13%"=="O" ( if "%f14%"=="O" ( if "%f15%"=="O" ( if "%f16%"=="O" goto victory2 
)
)
)
if "%f14%"=="O" ( if "%f15%"=="O" ( if "%f16%"=="O" ( if "%f17%"=="O" goto victory2 
)
)
)
if "%f15%"=="O" ( if "%f16%"=="O" ( if "%f17%"=="O" ( if "%f18%"=="O" goto victory2 
)
)
)
if "%f21%"=="O" ( if "%f22%"=="O" ( if "%f23%"=="O" ( if "%f24%"=="O" goto victory2 
)
)
)
if "%f22%"=="O" ( if "%f23%"=="O" ( if "%f24%"=="O" ( if "%f25%"=="O" goto victory2 
)
)
)
if "%f23%"=="O" ( if "%f24%"=="O" ( if "%f25%"=="O" ( if "%f26%"=="O" goto victory2 
)
)
)
if "%f24%"=="O" ( if "%f25%"=="O" ( if "%f26%"=="O" ( if "%f27%"=="O" goto victory2 
)
)
)
if "%f25%"=="O" ( if "%f26%"=="O" ( if "%f27%"=="O" ( if "%f28%"=="O" goto victory2 
)
)
)
if "%f31%"=="O" ( if "%f32%"=="O" ( if "%f33%"=="O" ( if "%f34%"=="O" goto victory2 
)
)
)
if "%f32%"=="O" ( if "%f33%"=="O" ( if "%f34%"=="O" ( if "%f35%"=="O" goto victory2 
)
)
)
if "%f33%"=="O" ( if "%f34%"=="O" ( if "%f35%"=="O" ( if "%f36%"=="O" goto victory2 
)
)
)
if "%f34%"=="O" ( if "%f35%"=="O" ( if "%f36%"=="O" ( if "%f37%"=="O" goto victory2 
)
)
)
if "%f35%"=="O" ( if "%f36%"=="O" ( if "%f37%"=="O" ( if "%f38%"=="O" goto victory2 
)
)
)
if "%f41%"=="O" ( if "%f42%"=="O" ( if "%f43%"=="O" ( if "%f44%"=="O" goto victory2 
)
)
)
if "%f42%"=="O" ( if "%f43%"=="O" ( if "%f44%"=="O" ( if "%f45%"=="O" goto victory2 
)
)
)
if "%f43%"=="O" ( if "%f44%"=="O" ( if "%f45%"=="O" ( if "%f46%"=="O" goto victory2 
)
)
)
if "%f44%"=="O" ( if "%f45%"=="O" ( if "%f46%"=="O" ( if "%f47%"=="O" goto victory2 
)
)
)
if "%f45%"=="O" ( if "%f46%"=="O" ( if "%f47%"=="O" ( if "%f48%"=="O" goto victory2 
)
)
)
if "%f51%"=="O" ( if "%f52%"=="O" ( if "%f53%"=="O" ( if "%f54%"=="O" goto victory2 
)
)
)
if "%f52%"=="O" ( if "%f53%"=="O" ( if "%f54%"=="O" ( if "%f55%"=="O" goto victory2 
)
)
)
if "%f53%"=="O" ( if "%f54%"=="O" ( if "%f55%"=="O" ( if "%f56%"=="O" goto victory2 
)
)
)
if "%f54%"=="O" ( if "%f55%"=="O" ( if "%f56%"=="O" ( if "%f57%"=="O" goto victory2 
)
)
)
if "%f55%"=="O" ( if "%f56%"=="O" ( if "%f57%"=="O" ( if "%f58%"=="O" goto victory2 
)
)
)
if "%f61%"=="O" ( if "%f62%"=="O" ( if "%f63%"=="O" ( if "%f64%"=="O" goto victory2 
)
)
)
if "%f62%"=="O" ( if "%f63%"=="O" ( if "%f64%"=="O" ( if "%f65%"=="O" goto victory2 
)
)
)
if "%f63%"=="O" ( if "%f64%"=="O" ( if "%f65%"=="O" ( if "%f66%"=="O" goto victory2 
)
)
)
if "%f64%"=="O" ( if "%f65%"=="O" ( if "%f66%"=="O" ( if "%f67%"=="O" goto victory2 
)
)
)
if "%f65%"=="O" ( if "%f66%"=="O" ( if "%f67%"=="O" ( if "%f68%"=="O" goto victory2 
)
)
)
if "%f71%"=="O" ( if "%f72%"=="O" ( if "%f73%"=="O" ( if "%f74%"=="O" goto victory2 
)
)
)
if "%f72%"=="O" ( if "%f73%"=="O" ( if "%f74%"=="O" ( if "%f75%"=="O" goto victory2 
)
)
)
if "%f73%"=="O" ( if "%f74%"=="O" ( if "%f75%"=="O" ( if "%f76%"=="O" goto victory2 
)
)
)
if "%f74%"=="O" ( if "%f75%"=="O" ( if "%f76%"=="O" ( if "%f77%"=="O" goto victory2 
)
)
)
if "%f75%"=="O" ( if "%f76%"=="O" ( if "%f77%"=="O" ( if "%f78%"=="O" goto victory2 
)
)
)
if "%f81%"=="O" ( if "%f82%"=="O" ( if "%f83%"=="O" ( if "%f84%"=="O" goto victory2 
)
)
)
if "%f82%"=="O" ( if "%f83%"=="O" ( if "%f84%"=="O" ( if "%f85%"=="O" goto victory2 
)
)
)
if "%f83%"=="O" ( if "%f84%"=="O" ( if "%f85%"=="O" ( if "%f86%"=="O" goto victory2 
)
)
)
if "%f84%"=="O" ( if "%f85%"=="O" ( if "%f86%"=="O" ( if "%f87%"=="O" goto victory2 
)
)
)
if "%f85%"=="O" ( if "%f86%"=="O" ( if "%f87%"=="O" ( if "%f88%"=="O" goto victory2 
)
)
)
if "%f11%"=="O" ( if "%f21%"=="O" ( if "%f31%"=="O" ( if "%f41%"=="O" goto victory2 
)
)
)
if "%f12%"=="O" ( if "%f22%"=="O" ( if "%f32%"=="O" ( if "%f42%"=="O" goto victory2 
)
)
)
if "%f13%"=="O" ( if "%f23%"=="O" ( if "%f33%"=="O" ( if "%f43%"=="O" goto victory2 
)
)
)
if "%f14%"=="O" ( if "%f24%"=="O" ( if "%f34%"=="O" ( if "%f44%"=="O" goto victory2 
)
)
)
if "%f15%"=="O" ( if "%f25%"=="O" ( if "%f35%"=="O" ( if "%f45%"=="O" goto victory2 
)
)
)
if "%f16%"=="O" ( if "%f26%"=="O" ( if "%f36%"=="O" ( if "%f46%"=="O" goto victory2 
)
)
)
if "%f17%"=="O" ( if "%f27%"=="O" ( if "%f37%"=="O" ( if "%f47%"=="O" goto victory2 
)
)
)
if "%f18%"=="O" ( if "%f28%"=="O" ( if "%f38%"=="O" ( if "%f48%"=="O" goto victory2 
)
)
)
if "%f51%"=="O" ( if "%f61%"=="O" ( if "%f71%"=="O" ( if "%f81%"=="O" goto victory2 
)
)
)
if "%f52%"=="O" ( if "%f62%"=="O" ( if "%f72%"=="O" ( if "%f82%"=="O" goto victory2 
)
)
)
if "%f53%"=="O" ( if "%f63%"=="O" ( if "%f73%"=="O" ( if "%f83%"=="O" goto victory2 
)
)
)
if "%f54%"=="O" ( if "%f64%"=="O" ( if "%f74%"=="O" ( if "%f84%"=="O" goto victory2 
)
)
)
if "%f55%"=="O" ( if "%f65%"=="O" ( if "%f75%"=="O" ( if "%f85%"=="O" goto victory2 
)
)
)
if "%f56%"=="O" ( if "%f66%"=="O" ( if "%f76%"=="O" ( if "%f86%"=="O" goto victory2 
)
)
)
if "%f57%"=="O" ( if "%f67%"=="O" ( if "%f77%"=="O" ( if "%f87%"=="O" goto victory2 
)
)
)
if "%f58%"=="O" ( if "%f68%"=="O" ( if "%f78%"=="O" ( if "%f88%"=="O" goto victory2 
)
)
)
if "%f11%"=="O" ( if "%f22%"=="O" ( if "%f33%"=="O" ( if "%f44%"=="O" goto victory2 
)
)
)
if "%f22%"=="O" ( if "%f33%"=="O" ( if "%f44%"=="O" ( if "%f55%"=="O" goto victory2 
)
)
)
if "%f33%"=="O" ( if "%f44%"=="O" ( if "%f55%"=="O" ( if "%f66%"=="O" goto victory2 
)
)
)
if "%f44%"=="O" ( if "%f55%"=="O" ( if "%f66%"=="O" ( if "%f77%"=="O" goto victory2 
)
)
)
if "%f55%"=="O" ( if "%f66%"=="O" ( if "%f77%"=="O" ( if "%f88%"=="O" goto victory2 
)
)
)
if "%f12%"=="O" ( if "%f23%"=="O" ( if "%f34%"=="O" ( if "%f45%"=="O" goto victory2 
)
)
)
if "%f23%"=="O" ( if "%f34%"=="O" ( if "%f45%"=="O" ( if "%f56%"=="O" goto victory2 
)
)
)
if "%f34%"=="O" ( if "%f45%"=="O" ( if "%f56%"=="O" ( if "%f67%"=="O" goto victory2 
)
)
)
if "%f45%"=="O" ( if "%f56%"=="O" ( if "%f67%"=="O" ( if "%f78%"=="O" goto victory2 
)
)
)
if "%f13%"=="O" ( if "%f24%"=="O" ( if "%f35%"=="O" ( if "%f46%"=="O" goto victory2 
)
)
)
if "%f24%"=="O" ( if "%f35%"=="O" ( if "%f46%"=="O" ( if "%f57%"=="O" goto victory2 
)
)
)
if "%f35%"=="O" ( if "%f46%"=="O" ( if "%f57%"=="O" ( if "%f68%"=="O" goto victory2 
)
)
)
if "%f14%"=="O" ( if "%f25%"=="O" ( if "%f36%"=="O" ( if "%f47%"=="O" goto victory2 
)
)
)
if "%f25%"=="O" ( if "%f36%"=="O" ( if "%f47%"=="O" ( if "%f58%"=="O" goto victory2 
)
)
)
if "%f15%"=="O" ( if "%f26%"=="O" ( if "%f37%"=="O" ( if "%f48%"=="O" goto victory2 
)
)
)
if "%f21%"=="O" ( if "%f32%"=="O" ( if "%f43%"=="O" ( if "%f54%"=="O" goto victory2 
)
)
)
if "%f32%"=="O" ( if "%f43%"=="O" ( if "%f54%"=="O" ( if "%f65%"=="O" goto victory2 
)
)
)
if "%f43%"=="O" ( if "%f54%"=="O" ( if "%f65%"=="O" ( if "%f76%"=="O" goto victory2 
)
)
)
if "%f54%"=="O" ( if "%f65%"=="O" ( if "%f76%"=="O" ( if "%f87%"=="O" goto victory2 
)
)
)
if "%f31%"=="O" ( if "%f42%"=="O" ( if "%f53%"=="O" ( if "%f64%"=="O" goto victory2 
)
)
)
if "%f42%"=="O" ( if "%f53%"=="O" ( if "%f64%"=="O" ( if "%f75%"=="O" goto victory2 
)
)
)
if "%f53%"=="O" ( if "%f64%"=="O" ( if "%f75%"=="O" ( if "%f86%"=="O" goto victory2 
)
)
)
if "%f32%"=="O" ( if "%f43%"=="O" ( if "%f54%"=="O" ( if "%f65%"=="O" goto victory2 
)
)
)
if "%f43%"=="O" ( if "%f54%"=="O" ( if "%f65%"=="O" ( if "%f76%"=="O" goto victory2 
)
)
)
if "%f54%"=="O" ( if "%f65%"=="O" ( if "%f76%"=="O" ( if "%f87%"=="O" goto victory2 
)
)
)
if "%f41%"=="O" ( if "%f52%"=="O" ( if "%f63%"=="O" ( if "%f74%"=="O" goto victory2 
)
)
)
if "%f52%"=="O" ( if "%f63%"=="O" ( if "%f74%"=="O" ( if "%f85%"=="O" goto victory2 
)
)
)
if "%f51%"=="O" ( if "%f62%"=="O" ( if "%f73%"=="O" ( if "%f84%"=="O" goto victory2 
)
)
)
if "%f81%"=="O" ( if "%f72%"=="O" ( if "%f63%"=="O" ( if "%f54%"=="O" goto victory2 
)
)
)
if "%f72%"=="O" ( if "%f63%"=="O" ( if "%f54%"=="O" ( if "%f45%"=="O" goto victory2 
)
)
)
if "%f63%"=="O" ( if "%f54%"=="O" ( if "%f45%"=="O" ( if "%f36%"=="O" goto victory2 
)
)
)
if "%f54%"=="O" ( if "%f45%"=="O" ( if "%f36%"=="O" ( if "%f27%"=="O" goto victory2 
)
)
)
if "%f45%"=="O" ( if "%f36%"=="O" ( if "%f27%"=="O" ( if "%f18%"=="O" goto victory2 
)
)
)
if "%f82%"=="O" ( if "%f73%"=="O" ( if "%f64%"=="O" ( if "%f55%"=="O" goto victory2 
)
)
)
if "%f73%"=="O" ( if "%f64%"=="O" ( if "%f55%"=="O" ( if "%f46%"=="O" goto victory2 
)
)
)
if "%f64%"=="O" ( if "%f55%"=="O" ( if "%f46%"=="O" ( if "%f37%"=="O" goto victory2 
)
)
)
if "%f55%"=="O" ( if "%f46%"=="O" ( if "%f37%"=="O" ( if "%f28%"=="O" goto victory2 
)
)
)
if "%f83%"=="O" ( if "%f74%"=="O" ( if "%f65%"=="O" ( if "%f56%"=="O" goto victory2 
)
)
)
if "%f74%"=="O" ( if "%f65%"=="O" ( if "%f56%"=="O" ( if "%f47%"=="O" goto victory2 
)
)
)
if "%f65%"=="O" ( if "%f56%"=="O" ( if "%f47%"=="O" ( if "%f38%"=="O" goto victory2 
)
)
)
if "%f84%"=="O" ( if "%f75%"=="O" ( if "%f66%"=="O" ( if "%f57%"=="O" goto victory2 
)
)
)
if "%f75%"=="O" ( if "%f66%"=="O" ( if "%f57%"=="O" ( if "%f48%"=="O" goto victory2 
)
)
)
if "%f85%"=="O" ( if "%f76%"=="O" ( if "%f67%"=="O" ( if "%f58%"=="O" goto victory2 
)
)
)
if "%f71%"=="O" ( if "%f62%"=="O" ( if "%f53%"=="O" ( if "%f44%"=="O" goto victory2 
)
)
)
if "%f62%"=="O" ( if "%f53%"=="O" ( if "%f44%"=="O" ( if "%f35%"=="O" goto victory2 
)
)
)
if "%f53%"=="O" ( if "%f44%"=="O" ( if "%f35%"=="O" ( if "%f26%"=="O" goto victory2 
)
)
)
if "%f44%"=="O" ( if "%f35%"=="O" ( if "%f26%"=="O" ( if "%f17%"=="O" goto victory2 
)
)
)
if "%f61%"=="O" ( if "%f52%"=="O" ( if "%f43%"=="O" ( if "%f34%"=="O" goto victory2 
)
)
)
if "%f52%"=="O" ( if "%f43%"=="O" ( if "%f35%"=="O" ( if "%f26%"=="O" goto victory2 
)
)
)
if "%f43%"=="O" ( if "%f35%"=="O" ( if "%f26%"=="O" ( if "%f17%"=="O" goto victory2 
)
)
)
if "%f51%"=="O" ( if "%f42%"=="O" ( if "%f33%"=="O" ( if "%f24%"=="O" goto victory2 
)
)
)
if "%f42%"=="O" ( if "%f33%"=="O" ( if "%f24%"=="O" ( if "%f15%"=="O" goto victory2 
)
)
)
if "%f41%"=="O" ( if "%f32%"=="O" ( if "%f23%"=="O" ( if "%f14%"=="O" goto victory2 
)
)
)
rem Unentschieden
if not "%f18%"==" " ( if not "%f28%"==" " ( if not "%f38%"==" " ( if not "%f48%"==" " ( if not "%f58%"==" " ( if not "%f68%"==" " ( if not "%f78%"==" " ( if not "%f88%"==" " goto draw
)
)
)
)
)
)
)
if %turn%==1 goto turnPlayer1
if %turn%==2 goto turnPlayer2

:exit