@echo off

rem so that we can use variables in for loops
setlocal ENABLEDELAYEDEXPANSION

:title
echo    --------------
echo   / Vier  Gewinnt \
echo  /   MULTIPLAYER   \
echo  -------------------
echo.

:init
echo Type 1 if you want to make the first turn, or 2 for the second
echo Your opponent needs to type the other number!
set /p player=
if "%player%"=="1" (
set marker=X
) else ( 
set marker=O
set player=2
)
set markerEmpty=_
echo Now type the path where the score is going to be saved
echo Your opponent needs to type the same path, so you both need access to that path!
set /p filename=
set filename=%filename%\score.txt
echo Checking if file is writeable...
echo. > %filename%
echo Great! Let´s begin!

:variables
set f11=%markerEmpty%
set f12=%markerEmpty%
set f13=%markerEmpty%
set f14=%markerEmpty%
set f15=%markerEmpty%
set f16=%markerEmpty%
set f17=%markerEmpty%
set f18=%markerEmpty%
set f21=%markerEmpty%
set f22=%markerEmpty%
set f23=%markerEmpty%
set f24=%markerEmpty%
set f25=%markerEmpty%
set f26=%markerEmpty%
set f27=%markerEmpty%
set f28=%markerEmpty%
set f31=%markerEmpty%
set f32=%markerEmpty%
set f33=%markerEmpty%
set f34=%markerEmpty%
set f35=%markerEmpty%
set f36=%markerEmpty%
set f37=%markerEmpty%
set f38=%markerEmpty%
set f41=%markerEmpty%
set f42=%markerEmpty%
set f43=%markerEmpty%
set f44=%markerEmpty%
set f45=%markerEmpty%
set f46=%markerEmpty%
set f47=%markerEmpty%
set f48=%markerEmpty%
set f51=%markerEmpty%
set f52=%markerEmpty%
set f53=%markerEmpty%
set f54=%markerEmpty%
set f55=%markerEmpty%
set f56=%markerEmpty%
set f57=%markerEmpty%
set f58=%markerEmpty%
set f61=%markerEmpty%
set f62=%markerEmpty%
set f63=%markerEmpty%
set f64=%markerEmpty%
set f65=%markerEmpty%
set f66=%markerEmpty%
set f67=%markerEmpty%
set f68=%markerEmpty%
set f71=%markerEmpty%
set f72=%markerEmpty%
set f73=%markerEmpty%
set f74=%markerEmpty%
set f75=%markerEmpty%
set f76=%markerEmpty%
set f77=%markerEmpty%
set f78=%markerEmpty%
set f81=%markerEmpty%
set f82=%markerEmpty%
set f83=%markerEmpty%
set f84=%markerEmpty%
set f85=%markerEmpty%
set f86=%markerEmpty%
set f87=%markerEmpty%
set f88=%markerEmpty%
rem set player=1

:game
rem read whole file
set line=0
FOR /F "tokens=*" %%i IN (%filename%) DO (
if !line!==1 ( set f11=%%i)
if !line!==2 ( set f12=%%i)
if !line!==3 ( set f13=%%i)
if !line!==4 ( set f14=%%i)
if !line!==5 ( set f15=%%i)
if !line!==6 ( set f16=%%i)
if !line!==7 ( set f17=%%i)
if !line!==8 ( set f18=%%i)
if !line!==9 ( set f21=%%i)
if !line!==10 ( set f22=%%i)
if !line!==11 ( set f23=%%i)
if !line!==12 ( set f24=%%i)
if !line!==13 ( set f25=%%i)
if !line!==14 ( set f26=%%i)
if !line!==15 ( set f27=%%i)
if !line!==16 ( set f28=%%i)
if !line!==17 ( set f31=%%i)
if !line!==18 ( set f32=%%i)
if !line!==19 ( set f33=%%i)
if !line!==20 ( set f34=%%i)
if !line!==21 ( set f35=%%i)
if !line!==22 ( set f36=%%i)
if !line!==23 ( set f37=%%i)
if !line!==24 ( set f38=%%i)
if !line!==25 ( set f41=%%i)
if !line!==26 ( set f42=%%i)
if !line!==27 ( set f43=%%i)
if !line!==28 ( set f44=%%i)
if !line!==29 ( set f45=%%i)
if !line!==30 ( set f46=%%i)
if !line!==31 ( set f47=%%i)
if !line!==32 ( set f48=%%i)
if !line!==33 ( set f51=%%i)
if !line!==34 ( set f52=%%i)
if !line!==35 ( set f53=%%i)
if !line!==36 ( set f54=%%i)
if !line!==37 ( set f55=%%i)
if !line!==38 ( set f56=%%i)
if !line!==39 ( set f57=%%i)
if !line!==40 ( set f58=%%i)
if !line!==41 ( set f61=%%i)
if !line!==42 ( set f62=%%i)
if !line!==43 ( set f63=%%i)
if !line!==44 ( set f64=%%i)
if !line!==45 ( set f65=%%i)
if !line!==46 ( set f66=%%i)
if !line!==47 ( set f67=%%i)
if !line!==48 ( set f68=%%i)
if !line!==49 ( set f71=%%i)
if !line!==50 ( set f72=%%i)
if !line!==51 ( set f73=%%i)
if !line!==52 ( set f74=%%i)
if !line!==53 ( set f75=%%i)
if !line!==54 ( set f76=%%i)
if !line!==55 ( set f77=%%i)
if !line!==56 ( set f78=%%i)
if !line!==57 ( set f81=%%i)
if !line!==58 ( set f82=%%i)
if !line!==59 ( set f83=%%i)
if !line!==60 ( set f84=%%i)
if !line!==61 ( set f85=%%i)
if !line!==62 ( set f86=%%i)
if !line!==63 ( set f87=%%i)
if !line!==64 ( set f88=%%i)
set /a line=line+1
)
rem read whole file end
:displayBoard
cls
color 07
rem echo %p1% X vs O %p2%
echo.
echo    1   2   3   4   5   6   7   8
echo  ^| %f18% ^| %f28% ^| %f38% ^| %f48% ^| %f58% ^| %f68% ^| %f78% ^| %f88% ^|
echo  ^|   ^|   ^|   ^|   ^|   ^|   ^|   ^|   ^|
echo  ^| %f17% ^| %f27% ^| %f37% ^| %f47% ^| %f57% ^| %f67% ^| %f77% ^| %f87% ^|
echo  ^|   ^|   ^|   ^|   ^|   ^|   ^|   ^|   ^|
echo  ^| %f16% ^| %f26% ^| %f36% ^| %f46% ^| %f56% ^| %f66% ^| %f76% ^| %f86% ^|
echo  ^|   ^|   ^|   ^|   ^|   ^|   ^|   ^|   ^|
echo  ^| %f15% ^| %f25% ^| %f35% ^| %f45% ^| %f55% ^| %f65% ^| %f75% ^| %f85% ^|
echo  ^|   ^|   ^|   ^|   ^|   ^|   ^|   ^|   ^|
echo  ^| %f14% ^| %f24% ^| %f34% ^| %f44% ^| %f54% ^| %f64% ^| %f74% ^| %f84% ^|
echo  ^|   ^|   ^|   ^|   ^|   ^|   ^|   ^|   ^|
echo  ^| %f13% ^| %f23% ^| %f33% ^| %f43% ^| %f53% ^| %f63% ^| %f73% ^| %f83% ^|
echo  ^|   ^|   ^|   ^|   ^|   ^|   ^|   ^|   ^|
echo  ^| %f12% ^| %f22% ^| %f32% ^| %f42% ^| %f52% ^| %f62% ^| %f72% ^| %f82% ^|
echo  ^|   ^|   ^|   ^|   ^|   ^|   ^|   ^|   ^|
echo  ^| %f11% ^| %f21% ^| %f31% ^| %f41% ^| %f51% ^| %f61% ^| %f71% ^| %f81% ^|
echo   -------------------------------
echo.
goto checkVictory

:writeToFile
echo %marker%> %filename%
echo %f11%>> %filename%
echo %f12%>> %filename%
echo %f13%>> %filename%
echo %f14%>> %filename%
echo %f15%>> %filename%
echo %f16%>> %filename%
echo %f17%>> %filename%
echo %f18%>> %filename%
echo %f21%>> %filename%
echo %f22%>> %filename%
echo %f23%>> %filename%
echo %f24%>> %filename%
echo %f25%>> %filename%
echo %f26%>> %filename%
echo %f27%>> %filename%
echo %f28%>> %filename%
echo %f31%>> %filename%
echo %f32%>> %filename%
echo %f33%>> %filename%
echo %f34%>> %filename%
echo %f35%>> %filename%
echo %f36%>> %filename%
echo %f37%>> %filename%
echo %f38%>> %filename%
echo %f41%>> %filename%
echo %f42%>> %filename%
echo %f43%>> %filename%
echo %f44%>> %filename%
echo %f45%>> %filename%
echo %f46%>> %filename%
echo %f47%>> %filename%
echo %f48%>> %filename%
echo %f51%>> %filename%
echo %f52%>> %filename%
echo %f53%>> %filename%
echo %f54%>> %filename%
echo %f55%>> %filename%
echo %f56%>> %filename%
echo %f57%>> %filename%
echo %f58%>> %filename%
echo %f61%>> %filename%
echo %f62%>> %filename%
echo %f63%>> %filename%
echo %f64%>> %filename%
echo %f65%>> %filename%
echo %f66%>> %filename%
echo %f67%>> %filename%
echo %f68%>> %filename%
echo %f71%>> %filename%
echo %f72%>> %filename%
echo %f73%>> %filename%
echo %f74%>> %filename%
echo %f75%>> %filename%
echo %f76%>> %filename%
echo %f77%>> %filename%
echo %f78%>> %filename%
echo %f81%>> %filename%
echo %f82%>> %filename%
echo %f83%>> %filename%
echo %f84%>> %filename%
echo %f85%>> %filename%
echo %f86%>> %filename%
echo %f87%>> %filename%
echo %f88%>> %filename%
goto waitForOppTurn

:waitForOppTurn
echo waiting for opponent
:waitForOppTurnSkipMsg
set line=0
FOR /F "tokens=*" %%i IN (%filename%) DO (
rem if "%line%"=="0" (
if !line!==0 (
set lineOne=%%i
if not !lineOne!==%marker% (
if !lineOne!==win ( goto victory2 )
if !lineOne!==draw ( goto draw )
set player=1
goto game
)
set /a line=line+1
)
)
ping localhost -n 8 > nul
goto waitForOppTurnSkipMsg

:yourTurn
color 17
rem echo %p1% %marker% places
echo Your turn (%marker%)
echo Select column (1-8)
set /p col=
if %col%==1 ( if %f18%==%markerEmpty% ( set f18=%marker% 
if %f17%==%markerEmpty% (
set f18=%markerEmpty%
set f17=%marker%
if %f16%==%markerEmpty% (
set f17=%markerEmpty%
set f16=%marker%
if %f15%==%markerEmpty% (
set f16=%markerEmpty%
set f15=%marker%
if %f14%==%markerEmpty% (
set f15=%markerEmpty%
set f14=%marker%
if %f13%==%markerEmpty% (
set f14=%markerEmpty%
set f13=%marker%
if %f12%==%markerEmpty% (
set f13=%markerEmpty%
set f12=%marker%
if %f11%==%markerEmpty% (
set f12=%markerEmpty%
set f11=%marker%
)
)
)
)
)
)
)
)
)
if %col%==2 ( if %f28%==%markerEmpty% ( set f28=%marker%
if %f27%==%markerEmpty% (
set f28=%markerEmpty%
set f27=%marker%
if %f26%==%markerEmpty% (
set f27=%markerEmpty%
set f26=%marker%
if %f25%==%markerEmpty% (
set f26=%markerEmpty%
set f25=%marker%
if %f24%==%markerEmpty% (
set f25=%markerEmpty%
set f24=%marker%
if %f23%==%markerEmpty% (
set f24=%markerEmpty%
set f23=%marker%
if %f22%==%markerEmpty% (
set f23=%markerEmpty%
set f22=%marker%
if %f21%==%markerEmpty% (
set f22=%markerEmpty%
set f21=%marker%
)
)
)
)
)
)
)
)
)
if %col%==3 ( if %f38%==%markerEmpty% ( set f38=%marker%
if %f37%==%markerEmpty% (
set f38=%markerEmpty%
set f37=%marker%
if %f36%==%markerEmpty% (
set f37=%markerEmpty%
set f36=%marker%
if %f35%==%markerEmpty% (
set f36=%markerEmpty%
set f35=%marker%
if %f34%==%markerEmpty% (
set f35=%markerEmpty%
set f34=%marker%
if %f33%==%markerEmpty% (
set f34=%markerEmpty%
set f33=%marker%
if %f32%==%markerEmpty% (
set f33=%markerEmpty%
set f32=%marker%
if %f31%==%markerEmpty% (
set f32=%markerEmpty%
set f31=%marker%
)
)
)
)
)
)
)
)
)
if %col%==4 ( if %f48%==%markerEmpty% ( set f48=%marker%
if %f47%==%markerEmpty% (
set f48=%markerEmpty%
set f47=%marker%
if %f46%==%markerEmpty% (
set f47=%markerEmpty%
set f46=%marker%
if %f45%==%markerEmpty% (
set f46=%markerEmpty%
set f45=%marker%
if %f44%==%markerEmpty% (
set f45=%markerEmpty%
set f44=%marker%
if %f43%==%markerEmpty% (
set f44=%markerEmpty%
set f43=%marker%
if %f42%==%markerEmpty% (
set f43=%markerEmpty%
set f42=%marker%
if %f41%==%markerEmpty% (
set f42=%markerEmpty%
set f41=%marker%
)
)
)
)
)
)
)
)
)
if %col%==5 ( if %f58%==%markerEmpty% ( set f58=%marker%
if %f57%==%markerEmpty% (
set f58=%markerEmpty%
set f57=%marker%
if %f56%==%markerEmpty% (
set f57=%markerEmpty%
set f56=%marker%
if %f55%==%markerEmpty% (
set f56=%markerEmpty%
set f55=%marker%
if %f54%==%markerEmpty% (
set f55=%markerEmpty%
set f54=%marker%
if %f53%==%markerEmpty% (
set f54=%markerEmpty%
set f53=%marker%
if %f52%==%markerEmpty% (
set f53=%markerEmpty%
set f52=%marker%
if %f51%==%markerEmpty% (
set f52=%markerEmpty%
set f51=%marker%
)
)
)
)
)
)
)
)
)
if %col%==6 ( if %f68%==%markerEmpty% ( set f68=%marker%
if %f67%==%markerEmpty% (
set f68=%markerEmpty%
set f67=%marker%
if %f66%==%markerEmpty% (
set f67=%markerEmpty%
set f66=%marker%
if %f65%==%markerEmpty% (
set f66=%markerEmpty%
set f65=%marker%
if %f64%==%markerEmpty% (
set f65=%markerEmpty%
set f64=%marker%
if %f63%==%markerEmpty% (
set f64=%markerEmpty%
set f63=%marker%
if %f62%==%markerEmpty% (
set f63=%markerEmpty%
set f62=%marker%
if %f61%==%markerEmpty% (
set f62=%markerEmpty%
set f61=%marker%
)
)
)
)
)
)
)
)
)
if %col%==7 ( if %f78%==%markerEmpty% ( set f78=%marker%
if %f77%==%markerEmpty% (
set f78=%markerEmpty%
set f77=%marker%
if %f76%==%markerEmpty% (
set f77=%markerEmpty%
set f76=%marker%
if %f75%==%markerEmpty% (
set f76=%markerEmpty%
set f75=%marker%
if %f74%==%markerEmpty% (
set f75=%markerEmpty%
set f74=%marker%
if %f73%==%markerEmpty% (
set f74=%markerEmpty%
set f73=%marker%
if %f72%==%markerEmpty% (
set f73=%markerEmpty%
set f72=%marker%
if %f71%==%markerEmpty% (
set f72=%markerEmpty%
set f71=%marker%
)
)
)
)
)
)
)
)
)
if %col%==8 ( if %f88%==%markerEmpty% ( set f88=%marker%
if %f87%==%markerEmpty% (
set f88=%markerEmpty%
set f87=%marker%
if %f86%==%markerEmpty% (
set f87=%markerEmpty%
set f86=%marker%
if %f85%==%markerEmpty% (
set f86=%markerEmpty%
set f85=%marker%
if %f84%==%markerEmpty% (
set f85=%markerEmpty%
set f84=%marker%
if %f83%==%markerEmpty% (
set f84=%markerEmpty%
set f83=%marker%
if %f82%==%markerEmpty% (
set f83=%markerEmpty%
set f82=%marker%
if %f81%==%markerEmpty% (
set f82=%markerEmpty%
set f81=%marker%
)
)
)
)
)
)
)
)
)
set player=2
rem goto writeToFile
goto displayBoard

:victory1
echo win> %filename%
echo Congratulations
echo You defeated your opponent
pause > nul
goto exit

:victory2
echo You lost the game :(
pause > nul
goto exit

:draw
echo draw> %filename%
echo A tie!
pause > nul
goto exit

:checkVictory
rem Player 1
if %f11%==%marker% ( if %f12%==%marker% ( if %f13%==%marker% ( if %f14%==%marker% goto victory1 
)
)
)
if %f12%==%marker% ( if %f13%==%marker% ( if %f14%==%marker% ( if %f15%==%marker% goto victory1 
)
)
)
if %f13%==%marker% ( if %f14%==%marker% ( if %f15%==%marker% ( if %f16%==%marker% goto victory1 
)
)
)
if %f14%==%marker% ( if %f15%==%marker% ( if %f16%==%marker% ( if %f17%==%marker% goto victory1 
)
)
)
if %f15%==%marker% ( if %f16%==%marker% ( if %f17%==%marker% ( if %f18%==%marker% goto victory1 
)
)
)
if %f21%==%marker% ( if %f22%==%marker% ( if %f23%==%marker% ( if %f24%==%marker% goto victory1 
)
)
)
if %f22%==%marker% ( if %f23%==%marker% ( if %f24%==%marker% ( if %f25%==%marker% goto victory1 
)
)
)
if %f23%==%marker% ( if %f24%==%marker% ( if %f25%==%marker% ( if %f26%==%marker% goto victory1 
)
)
)
if %f24%==%marker% ( if %f25%==%marker% ( if %f26%==%marker% ( if %f27%==%marker% goto victory1 
)
)
)
if %f25%==%marker% ( if %f26%==%marker% ( if %f27%==%marker% ( if %f28%==%marker% goto victory1 
)
)
)
if %f31%==%marker% ( if %f32%==%marker% ( if %f33%==%marker% ( if %f34%==%marker% goto victory1 
)
)
)
if %f32%==%marker% ( if %f33%==%marker% ( if %f34%==%marker% ( if %f35%==%marker% goto victory1 
)
)
)
if %f33%==%marker% ( if %f34%==%marker% ( if %f35%==%marker% ( if %f36%==%marker% goto victory1 
)
)
)
if %f34%==%marker% ( if %f35%==%marker% ( if %f36%==%marker% ( if %f37%==%marker% goto victory1 
)
)
)
if %f35%==%marker% ( if %f36%==%marker% ( if %f37%==%marker% ( if %f38%==%marker% goto victory1 
)
)
)
if %f41%==%marker% ( if %f42%==%marker% ( if %f43%==%marker% ( if %f44%==%marker% goto victory1 
)
)
)
if %f42%==%marker% ( if %f43%==%marker% ( if %f44%==%marker% ( if %f45%==%marker% goto victory1 
)
)
)
if %f43%==%marker% ( if %f44%==%marker% ( if %f45%==%marker% ( if %f46%==%marker% goto victory1 
)
)
)
if %f44%==%marker% ( if %f45%==%marker% ( if %f46%==%marker% ( if %f47%==%marker% goto victory1 
)
)
)
if %f45%==%marker% ( if %f46%==%marker% ( if %f47%==%marker% ( if %f48%==%marker% goto victory1 
)
)
)
if %f51%==%marker% ( if %f52%==%marker% ( if %f53%==%marker% ( if %f54%==%marker% goto victory1 
)
)
)
if %f52%==%marker% ( if %f53%==%marker% ( if %f54%==%marker% ( if %f55%==%marker% goto victory1 
)
)
)
if %f53%==%marker% ( if %f54%==%marker% ( if %f55%==%marker% ( if %f56%==%marker% goto victory1 
)
)
)
if %f54%==%marker% ( if %f55%==%marker% ( if %f56%==%marker% ( if %f57%==%marker% goto victory1 
)
)
)
if %f55%==%marker% ( if %f56%==%marker% ( if %f57%==%marker% ( if %f58%==%marker% goto victory1 
)
)
)
if %f61%==%marker% ( if %f62%==%marker% ( if %f63%==%marker% ( if %f64%==%marker% goto victory1 
)
)
)
if %f62%==%marker% ( if %f63%==%marker% ( if %f64%==%marker% ( if %f65%==%marker% goto victory1 
)
)
)
if %f63%==%marker% ( if %f64%==%marker% ( if %f65%==%marker% ( if %f66%==%marker% goto victory1 
)
)
)
if %f64%==%marker% ( if %f65%==%marker% ( if %f66%==%marker% ( if %f67%==%marker% goto victory1 
)
)
)
if %f65%==%marker% ( if %f66%==%marker% ( if %f67%==%marker% ( if %f68%==%marker% goto victory1 
)
)
)
if %f71%==%marker% ( if %f72%==%marker% ( if %f73%==%marker% ( if %f74%==%marker% goto victory1 
)
)
)
if %f72%==%marker% ( if %f73%==%marker% ( if %f74%==%marker% ( if %f75%==%marker% goto victory1 
)
)
)
if %f73%==%marker% ( if %f74%==%marker% ( if %f75%==%marker% ( if %f76%==%marker% goto victory1 
)
)
)
if %f74%==%marker% ( if %f75%==%marker% ( if %f76%==%marker% ( if %f77%==%marker% goto victory1 
)
)
)
if %f75%==%marker% ( if %f76%==%marker% ( if %f77%==%marker% ( if %f78%==%marker% goto victory1 
)
)
)
if %f81%==%marker% ( if %f82%==%marker% ( if %f83%==%marker% ( if %f84%==%marker% goto victory1 
)
)
)
if %f82%==%marker% ( if %f83%==%marker% ( if %f84%==%marker% ( if %f85%==%marker% goto victory1 
)
)
)
if %f83%==%marker% ( if %f84%==%marker% ( if %f85%==%marker% ( if %f86%==%marker% goto victory1 
)
)
)
if %f84%==%marker% ( if %f85%==%marker% ( if %f86%==%marker% ( if %f87%==%marker% goto victory1 
)
)
)
if %f85%==%marker% ( if %f86%==%marker% ( if %f87%==%marker% ( if %f88%==%marker% goto victory1 
)
)
)
if %f11%==%marker% ( if %f21%==%marker% ( if %f31%==%marker% ( if %f41%==%marker% goto victory1 
)
)
)
if %f12%==%marker% ( if %f22%==%marker% ( if %f32%==%marker% ( if %f42%==%marker% goto victory1 
)
)
)
if %f13%==%marker% ( if %f23%==%marker% ( if %f33%==%marker% ( if %f43%==%marker% goto victory1 
)
)
)
if %f14%==%marker% ( if %f24%==%marker% ( if %f34%==%marker% ( if %f44%==%marker% goto victory1 
)
)
)
if %f15%==%marker% ( if %f25%==%marker% ( if %f35%==%marker% ( if %f45%==%marker% goto victory1 
)
)
)
if %f16%==%marker% ( if %f26%==%marker% ( if %f36%==%marker% ( if %f46%==%marker% goto victory1 
)
)
)
if %f17%==%marker% ( if %f27%==%marker% ( if %f37%==%marker% ( if %f47%==%marker% goto victory1 
)
)
)
if %f18%==%marker% ( if %f28%==%marker% ( if %f38%==%marker% ( if %f48%==%marker% goto victory1 
)
)
)
if %f51%==%marker% ( if %f61%==%marker% ( if %f71%==%marker% ( if %f81%==%marker% goto victory1 
)
)
)
if %f52%==%marker% ( if %f62%==%marker% ( if %f72%==%marker% ( if %f82%==%marker% goto victory1 
)
)
)
if %f53%==%marker% ( if %f63%==%marker% ( if %f73%==%marker% ( if %f83%==%marker% goto victory1 
)
)
)
if %f54%==%marker% ( if %f64%==%marker% ( if %f74%==%marker% ( if %f84%==%marker% goto victory1 
)
)
)
if %f55%==%marker% ( if %f65%==%marker% ( if %f75%==%marker% ( if %f85%==%marker% goto victory1 
)
)
)
if %f56%==%marker% ( if %f66%==%marker% ( if %f76%==%marker% ( if %f86%==%marker% goto victory1 
)
)
)
if %f57%==%marker% ( if %f67%==%marker% ( if %f77%==%marker% ( if %f87%==%marker% goto victory1 
)
)
)
if %f58%==%marker% ( if %f68%==%marker% ( if %f78%==%marker% ( if %f88%==%marker% goto victory1 
)
)
)
if %f11%==%marker% ( if %f22%==%marker% ( if %f33%==%marker% ( if %f44%==%marker% goto victory1 
)
)
)
if %f22%==%marker% ( if %f33%==%marker% ( if %f44%==%marker% ( if %f55%==%marker% goto victory1 
)
)
)
if %f33%==%marker% ( if %f44%==%marker% ( if %f55%==%marker% ( if %f66%==%marker% goto victory1 
)
)
)
if %f44%==%marker% ( if %f55%==%marker% ( if %f66%==%marker% ( if %f77%==%marker% goto victory1 
)
)
)
if %f55%==%marker% ( if %f66%==%marker% ( if %f77%==%marker% ( if %f88%==%marker% goto victory1 
)
)
)
if %f12%==%marker% ( if %f23%==%marker% ( if %f34%==%marker% ( if %f45%==%marker% goto victory1 
)
)
)
if %f23%==%marker% ( if %f34%==%marker% ( if %f45%==%marker% ( if %f56%==%marker% goto victory1 
)
)
)
if %f34%==%marker% ( if %f45%==%marker% ( if %f56%==%marker% ( if %f67%==%marker% goto victory1 
)
)
)
if %f45%==%marker% ( if %f56%==%marker% ( if %f67%==%marker% ( if %f78%==%marker% goto victory1 
)
)
)
if %f13%==%marker% ( if %f24%==%marker% ( if %f35%==%marker% ( if %f46%==%marker% goto victory1 
)
)
)
if %f24%==%marker% ( if %f35%==%marker% ( if %f46%==%marker% ( if %f57%==%marker% goto victory1 
)
)
)
if %f35%==%marker% ( if %f46%==%marker% ( if %f57%==%marker% ( if %f68%==%marker% goto victory1 
)
)
)
if %f14%==%marker% ( if %f25%==%marker% ( if %f36%==%marker% ( if %f47%==%marker% goto victory1 
)
)
)
if %f25%==%marker% ( if %f36%==%marker% ( if %f47%==%marker% ( if %f58%==%marker% goto victory1 
)
)
)
if %f15%==%marker% ( if %f26%==%marker% ( if %f37%==%marker% ( if %f48%==%marker% goto victory1 
)
)
)
if %f21%==%marker% ( if %f32%==%marker% ( if %f43%==%marker% ( if %f54%==%marker% goto victory1 
)
)
)
if %f32%==%marker% ( if %f43%==%marker% ( if %f54%==%marker% ( if %f65%==%marker% goto victory1 
)
)
)
if %f43%==%marker% ( if %f54%==%marker% ( if %f65%==%marker% ( if %f76%==%marker% goto victory1 
)
)
)
if %f54%==%marker% ( if %f65%==%marker% ( if %f76%==%marker% ( if %f87%==%marker% goto victory1 
)
)
)
if %f31%==%marker% ( if %f42%==%marker% ( if %f53%==%marker% ( if %f64%==%marker% goto victory1 
)
)
)
if %f42%==%marker% ( if %f53%==%marker% ( if %f64%==%marker% ( if %f75%==%marker% goto victory1 
)
)
)
if %f53%==%marker% ( if %f64%==%marker% ( if %f75%==%marker% ( if %f86%==%marker% goto victory1 
)
)
)
if %f32%==%marker% ( if %f43%==%marker% ( if %f54%==%marker% ( if %f65%==%marker% goto victory1 
)
)
)
if %f43%==%marker% ( if %f54%==%marker% ( if %f65%==%marker% ( if %f76%==%marker% goto victory1 
)
)
)
if %f54%==%marker% ( if %f65%==%marker% ( if %f76%==%marker% ( if %f87%==%marker% goto victory1 
)
)
)
if %f41%==%marker% ( if %f52%==%marker% ( if %f63%==%marker% ( if %f74%==%marker% goto victory1 
)
)
)
if %f52%==%marker% ( if %f63%==%marker% ( if %f74%==%marker% ( if %f85%==%marker% goto victory1 
)
)
)
if %f51%==%marker% ( if %f62%==%marker% ( if %f73%==%marker% ( if %f84%==%marker% goto victory1 
)
)
)
if %f81%==%marker% ( if %f72%==%marker% ( if %f63%==%marker% ( if %f54%==%marker% goto victory1 
)
)
)
if %f72%==%marker% ( if %f63%==%marker% ( if %f54%==%marker% ( if %f45%==%marker% goto victory1 
)
)
)
if %f63%==%marker% ( if %f54%==%marker% ( if %f45%==%marker% ( if %f36%==%marker% goto victory1 
)
)
)
if %f54%==%marker% ( if %f45%==%marker% ( if %f36%==%marker% ( if %f27%==%marker% goto victory1 
)
)
)
if %f45%==%marker% ( if %f36%==%marker% ( if %f27%==%marker% ( if %f18%==%marker% goto victory1 
)
)
)
if %f82%==%marker% ( if %f73%==%marker% ( if %f64%==%marker% ( if %f55%==%marker% goto victory1 
)
)
)
if %f73%==%marker% ( if %f64%==%marker% ( if %f55%==%marker% ( if %f46%==%marker% goto victory1 
)
)
)
if %f64%==%marker% ( if %f55%==%marker% ( if %f46%==%marker% ( if %f37%==%marker% goto victory1 
)
)
)
if %f55%==%marker% ( if %f46%==%marker% ( if %f37%==%marker% ( if %f28%==%marker% goto victory1 
)
)
)
if %f83%==%marker% ( if %f74%==%marker% ( if %f65%==%marker% ( if %f56%==%marker% goto victory1 
)
)
)
if %f74%==%marker% ( if %f65%==%marker% ( if %f56%==%marker% ( if %f47%==%marker% goto victory1 
)
)
)
if %f65%==%marker% ( if %f56%==%marker% ( if %f47%==%marker% ( if %f38%==%marker% goto victory1 
)
)
)
if %f84%==%marker% ( if %f75%==%marker% ( if %f66%==%marker% ( if %f57%==%marker% goto victory1 
)
)
)
if %f75%==%marker% ( if %f66%==%marker% ( if %f57%==%marker% ( if %f48%==%marker% goto victory1 
)
)
)
if %f85%==%marker% ( if %f76%==%marker% ( if %f67%==%marker% ( if %f58%==%marker% goto victory1 
)
)
)
if %f71%==%marker% ( if %f62%==%marker% ( if %f53%==%marker% ( if %f44%==%marker% goto victory1 
)
)
)
if %f62%==%marker% ( if %f53%==%marker% ( if %f44%==%marker% ( if %f35%==%marker% goto victory1 
)
)
)
if %f53%==%marker% ( if %f44%==%marker% ( if %f35%==%marker% ( if %f26%==%marker% goto victory1 
)
)
)
if %f44%==%marker% ( if %f35%==%marker% ( if %f26%==%marker% ( if %f17%==%marker% goto victory1 
)
)
)
if %f61%==%marker% ( if %f52%==%marker% ( if %f43%==%marker% ( if %f34%==%marker% goto victory1 
)
)
)
if %f52%==%marker% ( if %f43%==%marker% ( if %f35%==%marker% ( if %f26%==%marker% goto victory1 
)
)
)
if %f43%==%marker% ( if %f34%==%marker% ( if %f25%==%marker% ( if %f16%==%marker% goto victory1 
)
)
)
if %f51%==%marker% ( if %f42%==%marker% ( if %f33%==%marker% ( if %f24%==%marker% goto victory1 
)
)
)
if %f42%==%marker% ( if %f33%==%marker% ( if %f24%==%marker% ( if %f15%==%marker% goto victory1 
)
)
)
if %f41%==%marker% ( if %f32%==%marker% ( if %f23%==%marker% ( if %f14%==%marker% goto victory1 
)
)
)
rem Unentschieden
if not %f18%==%markerEmpty% ( if not %f28%==%markerEmpty% ( if not %f38%==%markerEmpty% ( if not %f48%==%markerEmpty% ( if not %f58%==%markerEmpty% ( if not %f68%==%markerEmpty% ( if not %f78%==%markerEmpty% ( if not %f88%==%markerEmpty% goto draw
)
)
)
)
)
)
)
if %player%==1 goto yourTurn
if %player%==2 goto writeToFile

:exit