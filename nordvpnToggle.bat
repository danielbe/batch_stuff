@echo off
rem above line prevents printing remarks and commands
rem except for "echo someWord" of course

rem TODO check errorlevel

rem start nordvpn-service if it is stopped and start the program
rem close and stop otherwise

echo.#######################
echo. start this batch file as administrator if you get an access denied error
echo.#######################

rem check the state of the service, write into variable "state"
FOR /F "tokens=* USEBACKQ" %%F IN (`sc query nordvpn-service ^| find "STATE"`) DO (
SET state=%%F
)

setLocal EnableDelayedExpansion

set stopped=STOPPED
set started=RUNNING

rem check if state contains "STOPPED" or "RUNNING"
if not "x!state:%stopped%=!"=="x%state%" (
echo service is stopped. trying to start...
net start nordvpn-service
echo trying to launch program...
start /b /d "C:\Program Files\NordVPN\" NordVPN.exe
) else (
if not "x!state:%started%=!"=="x%state%" (
echo trying to stop process...
taskkill /im nordvpn.exe /F
echo service is started. trying to stop...
net stop nordvpn-service
)
)

endlocal

rem stop script from closing automatically and do not print pause message
pause > nul